
const homeStart = document.getElementById('homeStart');
const bouttonStart = document.getElementById('bouttonStart');
const homeJeu = document.getElementById('homeJeu');
let boutonsProgression = document.getElementById('boutonsProgression');
const reglesJeu = document.getElementById('reglesJeu');

let widthFaim = 100; // largeur de progressBarFaim
let widthSoif = 75; 
let widthBesoins = 50; 
let widthJouer = 25; 

const faim = document.getElementById('progressBarFaim'); // à l'intérieur de l'interval j'appelle la fontion "faim" qui garde la valeur de "progressBarFaim" 
const soif = document.getElementById('progressBarSoif');
const besoins = document.getElementById('progressBarBesoins');
const jouer = document.getElementById('progressBarJouer');

let gameRestart = document.getElementById('gameRestart');
let gameOver = document.getElementById('gameOver');

//Boutton start

bouttonStart.addEventListener ('click', function(){
    homeStart.style.display = 'none';
    homeJeu.style.display = 'block';
    boutonsProgression.style.display = 'block';
    reglesJeu.style.display = 'block';

//Boutton restart    

gameRestart.addEventListener ('click', function(){
    homeStart.style.display = 'none';
    document.location.reload();

})


// Jeu commence après avoir appuyé sur le bouton "start", barres de progression diminuent

    let faimInterval = setInterval(function(){ //ça s'execute chaque seconde (2 secondes = 1000)  
    isGameOver();// fait appel de la fonction isGameOver 
    faim.style.width = widthFaim + '%'; //style de ma parogress-bar 
    faim.innerHTML = widthFaim + '%'; //ca change la valeur de la barre de progression
    widthFaim = widthFaim - 10; // largeur 100 se réduit chaque seconde à moins 10 
    if (widthFaim<=0){clearInterval(faim); // ça arrete l'execution de la fonction quand width < ou = 0
    } 
}   ,1000)

    let soifInterval = setInterval(function(){ 
        let soif = document.getElementById('progressBarSoif');
        isGameOver(); 
        soif.style.width = widthSoif + '%'; 
        soif.innerHTML = widthSoif + '%'; 
        widthSoif = widthSoif - 5; 
        if (widthSoif<=0){clearInterval(soif); 
        } 
    }, 1000)

    let besoinsInterval = setInterval(function(){ 
        let besoins = document.getElementById('progressBarBesoins');  
        isGameOver();
        besoins.style.width = widthBesoins + '%'; 
        besoins.innerHTML = widthBesoins + '%'; 
        widthBesoins = widthBesoins - 10; 
        if (widthBesoins<=0){clearInterval(besoins); 
        } 
    }, 1000)

    let jouerInterval = setInterval(function(){ 
        let jouer = document.getElementById('progressBarJouer');
        isGameOver();   
        jouer.style.width = widthJouer + '%'; 
        jouer.innerHTML = widthJouer + '%'; 
        widthJouer = widthJouer - 5; 
        if (widthJouer<=0){clearInterval(jouer); 
        } 
    }, 1000)
}
)

//Game over

function isGameOver (){
    if (widthFaim < 0 || widthSoif < 0 || widthBesoins < 0 || widthJouer < 0) {
        widthFaim = 0;
        widthSoif = 0;
        widthBesoins = 0;
        widthJouer = 0;
        gameOver.style.display = 'block';
        //document.location.reload();
        clearInterval();
    }       
}


//Boutton Faim

let buttonFaim = document.getElementById('buttonFaim');
function buttonFaimClick() {
    widthFaim = widthFaim + 10;
    if (widthFaim>=100) {widthFaim=100;} // widthFaim = widthFaim >= 100 ? 100 : widthFaim;
    faim.style.width = widthFaim + '%';
    faim.innerHTML = widthFaim + '%';  
} 
buttonFaim.addEventListener ('click', buttonFaimClick);


// Boutton soif

let buttonSoif = document.getElementById('buttonSoif');
function buttonSoifClick() {
    widthSoif = widthSoif + 10;
    if (widthSoif>=100) {widthSoif=100;} 
    soif.style.width = widthSoif + '%';
    soif.innerHTML = widthSoif + '%';  
} 
buttonSoif.addEventListener ('click', buttonSoifClick);


// Boutton besoins

let buttonBesoins = document.getElementById('buttonBesoins');
function buttonBesoinsClick() {
    widthBesoins = widthBesoins + 10;
    if (widthBesoins>=100) {widthBesoins=100;} 
    besoins.style.width = widthBesoins + '%';
    besoins.innerHTML = widthBesoins + '%';  
} 
buttonBesoins.addEventListener ('click', buttonBesoinsClick);


//Boutton jouer

let buttonJouer = document.getElementById('buttonJouer');
function buttonJouerClick() {
    widthJouer = widthJouer + 10;
    if (widthJouer>=100) {widthJouer=100;} 
    jouer.style.width = widthJouer + '%';
    jouer.innerHTML = widthJouer + '%';  
} 
buttonJouer.addEventListener ('click', buttonJouerClick);




